package com.sanservices.hn.services.config.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class BadRequestExceptionHandler implements ExceptionMapper<BadRequestException> {
    private static final Logger log = LoggerFactory.getLogger(BadRequestException.class);

    @Override
    public Response toResponse(BadRequestException exception) {
        log.info(exception.getMessage());
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
