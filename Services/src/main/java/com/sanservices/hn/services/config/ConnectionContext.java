package com.sanservices.hn.services.config;

import org.jetbrains.annotations.Contract;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionContext {
    private static ConnectionContext ourInstance = new ConnectionContext();

    private ConnectionContext() {
    }

    @Contract(pure = true)
    public static ConnectionContext getInstance() {
        return ourInstance;
    }

    public Connection openGeographyConnection() throws SQLException {
        return DriverManager.getConnection("");
    }
}
