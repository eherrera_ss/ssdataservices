package com.sanservices.hn.services.config.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServerErrorExceptionHandler implements ExceptionMapper<ServerErrorException> {
    private static final Logger log = LoggerFactory.getLogger(ServerErrorExceptionHandler.class);

    @Override
    public Response toResponse(ServerErrorException exception) {
        log.error(exception.getMessage(), exception);
        return Response.serverError().build();
    }
}
