package com.sanservices.hn.services.subsystems;

import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.searches.ContinentSearchTypeParam;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface GeographySubsystem {
    @NotNull Collection<Continent> getAllContinents();

    Continent findContinent(String identifier, ContinentSearchTypeParam searchType);
}
