package com.sanservices.hn.services.subsystems.impl;

import com.sanservices.hn.services.config.ConnectionContext;
import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import com.sanservices.hn.services.data.repositories.ContinentRepository;
import com.sanservices.hn.services.data.repositories.jdbc.JdbcContinentRepository;
import com.sanservices.hn.services.searches.ContinentSearchTypeParam;
import com.sanservices.hn.services.subsystems.GeographySubsystem;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public class GeographySubsystemImpl implements GeographySubsystem {

    @Inject
    private ConnectionContext context;

    @Override
    public @NotNull Collection<Continent> getAllContinents() {
        @NotNull Collection<Continent> continents;

        try (Connection connection = context.openGeographyConnection()) {
            ContinentRepository continentRepository = new JdbcContinentRepository(connection);

            continents = continentRepository.getAll();
        } catch (SQLException | SSDataException e) {
            throw new ServerErrorException(500, e);
        }

        return continents;
    }

    @Override
    public Continent findContinent(String identifier, ContinentSearchTypeParam searchType) {
        @Nullable Continent continent;

        try (Connection connection = context.openGeographyConnection()) {
            ContinentRepository continentRepository = new JdbcContinentRepository(connection);

            switch (searchType) {
                case ID:
                    try {
                        continent = continentRepository.getById(Integer.valueOf(identifier));
                    } catch (NumberFormatException nfe) {
                        throw new BadRequestException(nfe);
                    }
                    break;

                case NAME:
                    continent = continentRepository.getByName(identifier);
                    break;

                default:
                    throw new BadRequestException(identifier + " not recognized.");
            }
        } catch (SQLException | SSDataException e) {
            throw new ServerErrorException(500, e);
        }

        if (continent == null) {
            throw new NotFoundException(identifier);
        }
        return continent;
    }
}
