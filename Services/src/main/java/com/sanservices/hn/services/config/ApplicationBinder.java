package com.sanservices.hn.services.config;

import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.data.internal.entities.ContinentImpl;
import com.sanservices.hn.services.searches.ContinentSearchTypeParam;
import com.sanservices.hn.services.subsystems.GeographySubsystem;
import org.glassfish.jersey.internal.inject.AbstractBinder;
import org.jetbrains.annotations.NotNull;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.util.Arrays;
import java.util.Collection;

public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(new GeographySubsystem() {
            @Override
            public @NotNull Collection<Continent> getAllContinents() {
                return Arrays.asList(new ContinentImpl(1, "America"), new ContinentImpl(2, "Europe"));
            }

            @Override
            public Continent findContinent(String identifier, ContinentSearchTypeParam searchType) {
                return getAllContinents().stream()
                        .filter(it -> {
                            switch (searchType) {
                                case ID:
                                    return Integer.valueOf(identifier) == it.getId();
                                case NAME:
                                    return it.getName().equalsIgnoreCase(identifier);
                                default:
                                    throw new BadRequestException(identifier);
                            }
                        })
                        .findFirst()
                        .orElseThrow(NotFoundException::new);
            }
        }).to(GeographySubsystem.class);

        bind(ConnectionContext.getInstance()).to(ConnectionContext.class);
    }
}
