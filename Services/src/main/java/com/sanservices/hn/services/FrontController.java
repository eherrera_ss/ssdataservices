package com.sanservices.hn.services;

import com.sanservices.hn.services.config.ApplicationBinder;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/")
public class FrontController extends ResourceConfig {
    private static final String CONTROLLERS_PACKAGE_PREFIX = ".controllers";
    private static final String HANDLERS_PACKAGE_PREFIX = ".config.handlers";

    public FrontController() {
        register(new ApplicationBinder());

        packages(this.getClass().getPackage().getName() + CONTROLLERS_PACKAGE_PREFIX);
        packages(this.getClass().getPackage().getName() + HANDLERS_PACKAGE_PREFIX);
    }
}
