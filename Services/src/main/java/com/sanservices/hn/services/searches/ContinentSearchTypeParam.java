package com.sanservices.hn.services.searches;

import org.jetbrains.annotations.Nullable;

public enum ContinentSearchTypeParam {
    ID, NAME;

    public static ContinentSearchTypeParam fromString(@Nullable String string) {
        if (string == null) return null;

        ContinentSearchTypeParam param;
        if (string.equalsIgnoreCase("id")) {
            param = ID;
        } else if (string.equalsIgnoreCase("name")) {
            param = NAME;
        } else {
            param = null;
        }

        return param;
    }
}
