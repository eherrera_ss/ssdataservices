package com.sanservices.hn.services.config.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnmappedExceptionHandler implements ExceptionMapper<Exception> {
    private static final Logger log = LoggerFactory.getLogger(UnmappedExceptionHandler.class);

    @Override
    public Response toResponse(Exception exception) {
        log.error(exception.getLocalizedMessage(), exception);
        return Response.serverError().build();
    }
}
