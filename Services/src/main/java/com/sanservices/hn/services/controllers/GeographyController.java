package com.sanservices.hn.services.controllers;

import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.searches.ContinentSearchTypeParam;
import com.sanservices.hn.services.subsystems.GeographySubsystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/Geography")
public class GeographyController {
    private static final Logger log = LoggerFactory.getLogger(GeographyController.class);

    @Inject
    private GeographySubsystem geographySubsystem;

    @GET
    @Path("/Continents")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Continent> getAllContinents() {
        log.info("/Continents called.");
        return geographySubsystem.getAllContinents();
    }

    @GET
    @Path("/Continents/{identifier}")
    @Produces(MediaType.APPLICATION_JSON)
    public Continent getContinentByIdentifier(@PathParam("identifier")
                                                      String identifier,
                                              @QueryParam("searchType") @DefaultValue("id")
                                                      ContinentSearchTypeParam searchType) {

        log.info("/Continents/" + identifier + " called");
        return geographySubsystem.findContinent(identifier, searchType);
    }
}
