package com.sanservices.hn.services.config.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class NotFoundExceptionHandler implements ExceptionMapper<NotFoundException> {
    private static final Logger log = LoggerFactory.getLogger(NotFoundExceptionHandler.class);

    @Override
    public Response toResponse(NotFoundException exception) {
        log.info(exception.getMessage());
        return Response.status(Response.Status.NOT_FOUND).build();
    }
}
