package com.sanservices.hn.services.data.internal.entities;

import com.sanservices.hn.services.data.entitites.Continent;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

public final class ContinentImpl implements Continent, Serializable {
    private static final long serialVersionUID = 6211678369034970002L;

    private final int id;
    private final @NotNull String name;

    public ContinentImpl(int id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContinentImpl continent = (ContinentImpl) o;
        return getId() == continent.getId() &&
               Objects.equals(getName(), continent.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @NotNull
    @Override
    public String toString() {
        return "ContinentImpl{" +
               "id=" + id +
               ", name='" + name + '\'' +
               '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }
}
