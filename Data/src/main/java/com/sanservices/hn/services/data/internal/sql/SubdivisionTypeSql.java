package com.sanservices.hn.services.data.internal.sql;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum SubdivisionTypeSql implements Sql {
    SELECT_ALL_SUBDIVISION_TYPES("SELECT SubTypId AS SubdivisionTypeId, Type " +
                                 "FROM SubdivisionType"),

    SELECT_SUBDIVISION_TYPE_BY_ID("SELECT SubTypId AS SubdivisionTypeId, Type " +
                                  "FROM SubdivisionType " +
                                  "WHERE SubdivisionTypeId = ?");

    private final @NotNull String statement;

    SubdivisionTypeSql(@NotNull String statement) {
        this.statement = statement;
    }

    @Contract(pure = true)
    @NotNull
    @Override
    public String getStatement() {
        return statement;
    }
}
