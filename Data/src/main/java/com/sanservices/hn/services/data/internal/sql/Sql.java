package com.sanservices.hn.services.data.internal.sql;

import org.jetbrains.annotations.NotNull;

public interface Sql {
    @NotNull String getStatement();
}
