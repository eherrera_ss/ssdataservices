package com.sanservices.hn.services.data.repositories.jdbc;

import com.sanservices.hn.services.data.entitites.Country;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import com.sanservices.hn.services.data.internal.entities.CountryImpl;
import com.sanservices.hn.services.data.repositories.CountryRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import static com.sanservices.hn.services.data.internal.sql.CountrySql.*;

public final class JdbcCountryRepository extends JdbcRepository implements CountryRepository {
    public JdbcCountryRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Collection<Country> getAll() throws SSDataException {
        Collection<Country> countries = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_ALL_COUNTRIES);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                countries.add(extractCountry(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return countries;
    }

    @Override
    public @Nullable Country getById(int id) throws SSDataException {
        Country country;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_COUNTRY_BY_ID, id);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            country = resultSet.next() ? extractCountry(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return country;
    }

    @Override
    public @Nullable Country getByName(@NotNull String name) throws SSDataException {
        Country country;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_COUNTRY_BY_NAME, name);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            country = resultSet.next() ? extractCountry(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return country;
    }

    @Override
    public @Nullable Country getByCode(@NotNull String code) throws SSDataException {
        Country country;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_COUNTRY_BY_CODE, code);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            country = resultSet.next() ? extractCountry(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return country;
    }

    @Override
    public @NotNull Collection<Country> getByContinentId(int continentId) throws SSDataException {
        Collection<Country> countries = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_COUNTRIES_BY_CONTINENT_ID, continentId);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                countries.add(extractCountry(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return countries;
    }

    private @NotNull Country extractCountry(ResultSet resultSet) throws SQLException {
        return new CountryImpl(
                resultSet.getInt("CountryId"),
                resultSet.getString("CountryCode"),
                resultSet.getString("CountryName"),
                resultSet.getInt("ContinentId")
        );
    }
}
