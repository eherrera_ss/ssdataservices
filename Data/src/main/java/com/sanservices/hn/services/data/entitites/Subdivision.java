package com.sanservices.hn.services.data.entitites;

import org.jetbrains.annotations.NotNull;

public interface Subdivision {
    int getId();

    @NotNull String getCode();

    @NotNull String getName();

    int getCountryId();

    int getSubdivisionTypeId();
}
