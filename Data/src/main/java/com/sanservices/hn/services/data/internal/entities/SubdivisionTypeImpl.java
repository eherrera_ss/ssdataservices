package com.sanservices.hn.services.data.internal.entities;

import com.sanservices.hn.services.data.entitites.SubdivisionType;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

public final class SubdivisionTypeImpl implements SubdivisionType, Serializable {
    private static final long serialVersionUID = -2003400887727814503L;

    private final int id;
    private final @NotNull String name;

    public SubdivisionTypeImpl(int id, @NotNull String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubdivisionTypeImpl that = (SubdivisionTypeImpl) o;
        return getId() == that.getId() &&
               Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @NotNull
    @Override
    public String toString() {
        return "SubdivisionTypeImpl{" +
               "id=" + id +
               ", name='" + name + '\'' +
               '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }
}
