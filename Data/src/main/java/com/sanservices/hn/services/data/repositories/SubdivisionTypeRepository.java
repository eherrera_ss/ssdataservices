package com.sanservices.hn.services.data.repositories;

import com.sanservices.hn.services.data.entitites.SubdivisionType;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface SubdivisionTypeRepository {
    @NotNull
    Collection<SubdivisionType> getAll() throws SSDataException;

    @Nullable
    SubdivisionType getById(int id) throws SSDataException;
}
