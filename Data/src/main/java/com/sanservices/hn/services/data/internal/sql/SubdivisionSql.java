package com.sanservices.hn.services.data.internal.sql;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum SubdivisionSql implements Sql {

    // @formatter:off
    SELECT_ALL_SUBDIVISIONS("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                            "FROM Subdivision " +
                            "WHERE SubdivisionActive = 'T'"),

    SELECT_SUBDIVISION_BY_ID("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                             "FROM Subdivision " +
                             "WHERE SubdivisionActive = 'T' AND SubdivisionId = ?"),

    SELECT_SUBDIVISION_BY_NAME("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                    "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                               "FROM Subdivision " +
                               "WHERE SubdivisionActive = 'T' AND SubdivisionName = ?"),

    SELECT_SUBDIVISION_BY_CODE("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                    "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                               "FROM Subdivision " +
                               "WHERE SubdivisionActive = 'T' AND SubdivisionCode = ?"),

    SELECT_SUBDIVISIONS_BY_COUNTRY_ID("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                            "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                                      "FROM Subdivision " +
                                      "WHERE SubdivisionActive = 'T' AND SubdivisionId = ?"),

    SELECT_SUBDIVISIONS_BY_SUBDIVISION_TYPE_ID("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                               "CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                                               "FROM Subdivision " +
                                               "WHERE SubdivisionActive = 'T' AND SubdivisionTypeId = ?"),

    SELECT_SUBDIVISIONS_BY_COUNTRY_CODE("SELECT SubId AS SubdivisionId, SubdivisionCode, SubdivisionName, " +
                                        "Subdivision.CouId AS CountryId, SubTypId AS SubdivisionTypeId " +
                                        "FROM Subdivision subdivision" +
                                        "INNER JOIN Country ON Country.CountryId = Subdivision.CountryId " +
                                        "WHERE SubdivisionActive = 'T' AND CountryActive = 'T' AND CountryCode = ?");
    // @formatter:on

    private final @NotNull String statement;

    SubdivisionSql(@NotNull String statement) {
        this.statement = statement;
    }

    @Contract(pure = true)
    @NotNull
    @Override
    public String getStatement() {
        return statement;
    }
}
