package com.sanservices.hn.services.data.repositories.jdbc;

import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import com.sanservices.hn.services.data.internal.entities.ContinentImpl;
import com.sanservices.hn.services.data.repositories.ContinentRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import static com.sanservices.hn.services.data.internal.sql.ContinentSql.SELECT_ALL_CONTINENTS;
import static com.sanservices.hn.services.data.internal.sql.ContinentSql.SELECT_CONTINENT_BY_ID;
import static com.sanservices.hn.services.data.internal.sql.ContinentSql.SELECT_CONTINENT_BY_NAME;

public final class JdbcContinentRepository extends JdbcRepository implements ContinentRepository {

    public JdbcContinentRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Collection<Continent> getAll() throws SSDataException {
        Collection<Continent> continents = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_ALL_CONTINENTS);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                continents.add(extractContinent(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return continents;
    }

    @Override
    public @Nullable Continent getById(int id) throws SSDataException {
        Continent continent;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_CONTINENT_BY_ID, id);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            continent = resultSet.next() ? extractContinent(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return continent;
    }

    @Override
    public @Nullable Continent getByName(@NotNull String name) throws SSDataException {
        Continent continent;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_CONTINENT_BY_NAME, name);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            continent = resultSet.next() ? extractContinent(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return continent;
    }

    private @NotNull Continent extractContinent(@NotNull ResultSet resultSet) throws SQLException {
        return new ContinentImpl(
                resultSet.getInt("ContinentId"),
                resultSet.getString("ContinentName")
        );
    }
}
