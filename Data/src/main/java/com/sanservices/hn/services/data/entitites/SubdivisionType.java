package com.sanservices.hn.services.data.entitites;

import org.jetbrains.annotations.NotNull;

public interface SubdivisionType {
    int getId();

    @NotNull String getName();
}
