package com.sanservices.hn.services.data.repositories;

import com.sanservices.hn.services.data.entitites.Country;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface CountryRepository {
    @NotNull
    Collection<Country> getAll() throws SSDataException;

    @Nullable
    Country getById(int id) throws SSDataException;

    @Nullable
    Country getByName(@NotNull String name) throws SSDataException;

    @Nullable
    Country getByCode(@NotNull String code) throws SSDataException;

    @NotNull
    Collection<Country> getByContinentId(int continentId) throws SSDataException;
}
