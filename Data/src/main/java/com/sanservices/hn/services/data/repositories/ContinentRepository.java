package com.sanservices.hn.services.data.repositories;

import com.sanservices.hn.services.data.entitites.Continent;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ContinentRepository {

    @NotNull
    Collection<Continent> getAll() throws SSDataException;

    @Nullable
    Continent getById(int id) throws SSDataException;

    @Nullable
    Continent getByName(@NotNull String name) throws SSDataException;
}
