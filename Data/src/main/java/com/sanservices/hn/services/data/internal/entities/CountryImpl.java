package com.sanservices.hn.services.data.internal.entities;

import com.sanservices.hn.services.data.entitites.Country;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

public final class CountryImpl implements Country, Serializable {
    private static final long serialVersionUID = -1497408368325266268L;

    private final int id;
    private final @NotNull String code;
    private final @NotNull String name;
    private final int continentId;

    public CountryImpl(int id, @NotNull String code, @NotNull String name, int continentId) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.continentId = continentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryImpl country = (CountryImpl) o;
        return getId() == country.getId() &&
               getContinentId() == country.getContinentId() &&
               Objects.equals(getCode(), country.getCode()) &&
               Objects.equals(getName(), country.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCode(), getName(), getContinentId());
    }

    @NotNull
    @Override
    public String toString() {
        return "CountryImpl{" +
               "id=" + id +
               ", code='" + code + '\'' +
               ", name='" + name + '\'' +
               ", continentId=" + continentId +
               '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @NotNull
    @Override
    public String getCode() {
        return code;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getContinentId() {
        return continentId;
    }
}
