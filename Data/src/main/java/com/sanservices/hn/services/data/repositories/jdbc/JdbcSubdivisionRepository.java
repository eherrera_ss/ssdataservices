package com.sanservices.hn.services.data.repositories.jdbc;

import com.sanservices.hn.services.data.entitites.Subdivision;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import com.sanservices.hn.services.data.internal.entities.SubdivisionImpl;
import com.sanservices.hn.services.data.repositories.SubdivisionRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_ALL_SUBDIVISIONS;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISIONS_BY_COUNTRY_CODE;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISIONS_BY_COUNTRY_ID;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISIONS_BY_SUBDIVISION_TYPE_ID;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISION_BY_CODE;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISION_BY_ID;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionSql.SELECT_SUBDIVISION_BY_NAME;

public final class JdbcSubdivisionRepository extends JdbcRepository implements SubdivisionRepository {
    public JdbcSubdivisionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Collection<Subdivision> getAll() throws SSDataException {
        Collection<Subdivision> subdivisions = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_ALL_SUBDIVISIONS);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                subdivisions.add(extractSubdivision(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisions;
    }

    @Override
    public @Nullable Subdivision getById(int id) throws SSDataException {
        Subdivision subdivision;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISION_BY_ID, id);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            subdivision = resultSet.next() ? extractSubdivision(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivision;
    }

    @Override
    public @Nullable Subdivision getByName(@NotNull String name) throws SSDataException {
        Subdivision subdivision;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISION_BY_NAME, name);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            subdivision = resultSet.next() ? extractSubdivision(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivision;
    }

    @Override
    public @Nullable Subdivision getByCode(@NotNull String code) throws SSDataException {
        Subdivision subdivision;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISION_BY_CODE, code);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            subdivision = resultSet.next() ? extractSubdivision(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivision;
    }

    @Override
    public @NotNull Collection<Subdivision> getByCountryId(int countryId) throws SSDataException {
        Collection<Subdivision> subdivisions = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISIONS_BY_COUNTRY_ID, countryId);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                subdivisions.add(extractSubdivision(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisions;
    }

    @Override
    public @NotNull Collection<Subdivision> getByCountryCode(String countryCode) throws SSDataException {
        Collection<Subdivision> subdivisions = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISIONS_BY_COUNTRY_CODE, countryCode);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                subdivisions.add(extractSubdivision(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisions;
    }

    @Override
    public @NotNull Collection<Subdivision> getBySubdivisionTypeId(int subdivisionTypeId) throws SSDataException {
        Collection<Subdivision> subdivisions = new ArrayList<>();

        try (PreparedStatement preparedStatement =
                     prepareStatement(SELECT_SUBDIVISIONS_BY_SUBDIVISION_TYPE_ID, subdivisionTypeId);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            while (resultSet.next()) {
                subdivisions.add(extractSubdivision(resultSet));
            }
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisions;
    }

    private @NotNull Subdivision extractSubdivision(@NotNull ResultSet resultSet) throws SQLException {
        return new SubdivisionImpl(
                resultSet.getInt("SubdivisionId"),
                resultSet.getString("SubdivisionCode"),
                resultSet.getString("SubdivisionName"),
                resultSet.getInt("CountryId"),
                resultSet.getInt("SubdivisionTypeId")
        );
    }
}
