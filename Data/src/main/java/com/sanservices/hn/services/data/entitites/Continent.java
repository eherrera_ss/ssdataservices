package com.sanservices.hn.services.data.entitites;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public interface Continent extends Serializable {
    int getId();

    @NotNull String getName();
}
