package com.sanservices.hn.services.data.exceptions;

public class SSDataException extends RuntimeException {
    private static final long serialVersionUID = -5850980264963073670L;

    public SSDataException() {
    }

    public SSDataException(String message) {
        super(message);
    }

    public SSDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public SSDataException(Throwable cause) {
        super(cause);
    }

    public SSDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
