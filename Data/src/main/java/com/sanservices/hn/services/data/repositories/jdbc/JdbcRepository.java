package com.sanservices.hn.services.data.repositories.jdbc;

import com.sanservices.hn.services.data.internal.sql.Sql;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

class JdbcRepository {
    private final @NotNull Connection connection;

    JdbcRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    final PreparedStatement prepareStatement(@NotNull Sql sql, Object... params) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(sql.getStatement());
        for (int i = 0; i < params.length; i++) {
            statement.setObject(i + 1, params[i]);
        }
        return statement;
    }
}
