package com.sanservices.hn.services.data.repositories.jdbc;

import com.sanservices.hn.services.data.entitites.SubdivisionType;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import com.sanservices.hn.services.data.internal.entities.SubdivisionTypeImpl;
import com.sanservices.hn.services.data.repositories.SubdivisionTypeRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import static com.sanservices.hn.services.data.internal.sql.SubdivisionTypeSql.SELECT_ALL_SUBDIVISION_TYPES;
import static com.sanservices.hn.services.data.internal.sql.SubdivisionTypeSql.SELECT_SUBDIVISION_TYPE_BY_ID;

public final class JdbcSubdivisionTypeRepository extends JdbcRepository implements SubdivisionTypeRepository {
    public JdbcSubdivisionTypeRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Collection<SubdivisionType> getAll() throws SSDataException {
        Collection<SubdivisionType> subdivisionTypes = new ArrayList<>();

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_ALL_SUBDIVISION_TYPES);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            subdivisionTypes.add(extractSubdivisionType(resultSet));
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisionTypes;
    }

    @Override
    public @Nullable SubdivisionType getById(int id) throws SSDataException {
        SubdivisionType subdivisionType;

        try (PreparedStatement preparedStatement = prepareStatement(SELECT_SUBDIVISION_TYPE_BY_ID, id);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            subdivisionType = resultSet.next() ? extractSubdivisionType(resultSet) : null;
        } catch (SQLException e) {
            throw new SSDataException(e);
        }

        return subdivisionType;
    }

    private @NotNull SubdivisionType extractSubdivisionType(@NotNull ResultSet resultSet) throws SQLException {
        return new SubdivisionTypeImpl(
                resultSet.getInt("SubdivisionTypeId"),
                resultSet.getString("Type")
        );
    }
}
