package com.sanservices.hn.services.data.repositories;

import com.sanservices.hn.services.data.entitites.Subdivision;
import com.sanservices.hn.services.data.exceptions.SSDataException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface SubdivisionRepository {

    @NotNull
    Collection<Subdivision> getAll() throws SSDataException;

    @Nullable
    Subdivision getById(int id) throws SSDataException;

    @Nullable
    Subdivision getByName(@NotNull String name) throws SSDataException;

    @Nullable
    Subdivision getByCode(@NotNull String code) throws SSDataException;

    @NotNull
    Collection<Subdivision> getByCountryId(int countryId) throws SSDataException;

    @NotNull
    Collection<Subdivision> getByCountryCode(String countryCode) throws SSDataException;

    @NotNull
    Collection<Subdivision> getBySubdivisionTypeId(int subdivisionTypeId) throws SSDataException;
}
