package com.sanservices.hn.services.data.internal.entities;

import com.sanservices.hn.services.data.entitites.Subdivision;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

public final class SubdivisionImpl implements Subdivision, Serializable {
    private static final long serialVersionUID = 7279827053950930742L;

    private final int id;
    private final @NotNull String code;
    private final @NotNull String name;
    private final int countryId;
    private final int subdivisionTypeId;

    public SubdivisionImpl(int id, @NotNull String code, @NotNull String name, int countryId, int subdivisionTypeId) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.countryId = countryId;
        this.subdivisionTypeId = subdivisionTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubdivisionImpl that = (SubdivisionImpl) o;
        return getId() == that.getId() &&
               getCountryId() == that.getCountryId() &&
               getSubdivisionTypeId() == that.getSubdivisionTypeId() &&
               Objects.equals(getCode(), that.getCode()) &&
               Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCode(), getName(), getCountryId(), getSubdivisionTypeId());
    }

    @NotNull
    @Override
    public String toString() {
        return "SubdivisionImpl{" +
               "id=" + id +
               ", code='" + code + '\'' +
               ", name='" + name + '\'' +
               ", countryId=" + countryId +
               ", subdivisionTypeId=" + subdivisionTypeId +
               '}';
    }

    @Override
    public int getId() {
        return id;
    }

    @NotNull
    @Override
    public String getCode() {
        return code;
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getCountryId() {
        return countryId;
    }

    @Override
    public int getSubdivisionTypeId() {
        return subdivisionTypeId;
    }
}
