package com.sanservices.hn.services.data.internal.sql;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum ContinentSql implements Sql {
    SELECT_ALL_CONTINENTS("SELECT ConId AS ContinentId, ContinentName " +
                          "FROM Continent " +
                          "WHERE ContinentActive = 'T'"),

    SELECT_CONTINENT_BY_ID("SELECT ConId AS ContinentId, ContinentName " +
                           "FROM Continent " +
                           "WHERE ContinentActive = 'T' AND ContinentId = ?"),

    SELECT_CONTINENT_BY_NAME("SELECT ConId AS ContinentId, ContinentName " +
                             "FROM Continent " +
                             "WHERE ContinentActive = 'T' AND LOWER(ContinentName) = LOWER(?)");

    private final @NotNull String statement;

    ContinentSql(@NotNull String statement) {
        this.statement = statement;
    }

    @Override
    @Contract(pure = true)
    @NotNull
    public String getStatement() {
        return statement;
    }
}
