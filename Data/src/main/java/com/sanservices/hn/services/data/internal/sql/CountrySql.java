package com.sanservices.hn.services.data.internal.sql;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

public enum CountrySql implements Sql {
    SELECT_ALL_COUNTRIES("SELECT CouId AS CountryId, CountryCode, CountryName, ConId AS ContinentId " +
                         "FROM Country " +
                         "WHERE CountryActive = 'T'"),

    SELECT_COUNTRY_BY_ID("SELECT CouId AS CountryId, CountryCode, CountryName, ConId AS ContinentId " +
                         "FROM Country " +
                         "WHERE CountryActive = 'T' AND CountryId = ?"),

    SELECT_COUNTRY_BY_NAME("SELECT CouId AS CountryId, CountryCode, CountryName, ConId AS ContinentId " +
                           "FROM Country " +
                           "WHERE CountryActive = 'T' AND LOWER(CountryName) = LOWER(?)"),

    SELECT_COUNTRY_BY_CODE("SELECT CouId AS CountryId, CountryCode, CountryName, ConId AS ContinentId " +
                           "FROM Country " +
                           "WHERE CountryActive = 'T' AND LOWER(CountryCode) = LOWER(?)"),

    SELECT_COUNTRIES_BY_CONTINENT_ID("SELECT CouId AS CountryId, CountryCode, CountryName, ConId AS ContinentId " +
                                     "FROM Country " +
                                     "WHERE CountryActive = 'T' AND ContinentId = ?");

    private final @NotNull String statement;

    CountrySql(@NotNull String statement) {
        this.statement = statement;
    }

    @Contract(pure = true)
    @Override
    public @NotNull String getStatement() {
        return statement;
    }
}
